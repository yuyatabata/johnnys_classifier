## Johnny's_classifier概要
顔写真からジャニーズ系かどうか判定するアプリケーションです。
※残課題は残したまま、とりあえずアップしました。

### 機能
 ・学習用の画像はスクレイピングで収集
 ・顔写真のアップロード
 ・kerasのCNNで分類
 ・結果はHTMLで出力し、ブラウザで確認

### 残課題
 ・スレッドセーフにできていない
 ・判別が厳密すぎる
 ・見た目が素材そのまま
 ・サーバーに上げる